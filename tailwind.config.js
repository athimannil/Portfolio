/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    container: {
      center: true,
      padding: {
        DEFAULT: '1rem',
        sm: '1.5rem',
        lg: '2rem',
        // xl: '5rem',
        // '2xl': '6rem',
      },
    },
    extend: {
      screens: {
        'xs': '480px',
      },
      backgroundImage: {
        'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
        'gradient-conic':
          'conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))',
      },
      colors: {
        'primary': '#e16d65',
        'primary-lighten': '#dd5950',
        'primary-darken': '#dd5950',
        'secondary': '#e7e7e7',
      },
      animation: {
        'shaking': 'shaking 0.1s infinite',
      },
      keyframes: {
        shaking: {
          '0%': {
            transform: 'rotate(3deg)'
          },
          '50%': {
            transform: 'rotate(-3deg)'
          },
          '100%': {
            transform: 'rotate(3deg)'
          }
        }
      }
    },
  },
  plugins: [],
};
