'use client';
import Link from 'next/link';
// import { useRouter } from 'next/router';
import { usePathname } from 'next/navigation';

import './style.scss';

type NavLinksProps = {
  label: string;
  link: string;
}

const navLinks: NavLinksProps[] = [
  {
    label: 'home',
    link: ''
  },
  {
    label: 'about me',
    link: 'about-me'
  },
  {
    label: 'portfolio',
    link: 'portfolio'
  },
  {
    label: 'cv',
    link: 'resume'
  },
  {
    label: 'contact',
    link: 'contact'
  },
];

const Header = () => {
  const pathName = usePathname();

  return (
    <header className="header">
      <Link href="/" className="header__home"><h2>Athimannil</h2></Link>
      <input className="hidden peer" id="checkbox" type="checkbox" />
      <label className="header__menu" htmlFor="checkbox">
        <span className="header__menu-line"></span>
        <span className="header__menu-line"></span>
        <span className="header__menu-line"></span>
      </label>
      <nav className="header__nav">
        {navLinks.map(({ label, link }) =>
          <Link key={label} className={`header__nav-link ${link === pathName.substring(1) ? 'active' : ''}`} href={`/${link}`}>{label}</Link>)}
      </nav>
    </header>
  );
};

export default Header;
