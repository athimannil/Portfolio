import { FC } from 'react';
import { TestimonialProps } from './type';

import './style.scss';

const Testimonial: FC<TestimonialProps> = ({ message, name, position, profileLink }) => {
  // console.log('profileLink ', profileLink);
  return (
    <figure className="testimonial">
      <blockquote className="testimonial__message">
        <p className="testimonial__message-text">{message}</p>
      </blockquote>
      <figcaption className="testimonial__caption"> &mdash; <a className="testimonial__caption-name">{name}</a>
        <cite className="testimonial__caption-position"> | {position}</cite>
      </figcaption>
    </figure>
  );
};

export default Testimonial;
