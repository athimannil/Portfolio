import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { ThemeProvider } from '@emotion/react';
import { Theme } from '../../theme/colors';
import { InputTypeVariant } from './types';

import Input from './Input';

const inputVariants: {[key: string]: string} = {
  text: 'textbox',
  number: 'spinbutton',
  email: 'textbox',
  search: 'searchbox',
};

const inputSetup = (inputType: InputTypeVariant = 'text') => {
  const onChangeValue = jest.fn();
  render(
    <ThemeProvider theme={Theme}>
      <Input type={inputType} id="name" onChange={onChangeValue} placeholder="name" />);
    </ThemeProvider>
  );
  return screen.getByRole(inputVariants[inputType]) as HTMLInputElement;
};

describe('Input text', () => {
  it('input in matchng case', () => {
    const inputField = inputSetup('text');

    fireEvent.change(inputField, { target: { value: 'Athimannil' } });
    expect(inputField.value).toBe('Athimannil');
  });

  it('input text in miss matchng', () => {
    const inputField = inputSetup('text');

    fireEvent.change(inputField, { target: { value: 'Muhammed' } });
    expect(inputField.value).not.toBe('Athimannil');
  });
});

describe('Input text', () => {
  it('input in number', () => {
    const inputField = inputSetup('number');

    fireEvent.change(inputField, { target: { value: '1234567890' } });
    expect(inputField.value).toBe('1234567890');
  });

  it('input text in miss matchng', () => {
    const inputField = inputSetup('number');

    fireEvent.change(inputField, { target: { value: '1234567890' } });
    expect(inputField.value).not.toBe('0987654321');
  });
});

describe('Input email', () => {
  it('input in valid email', () => {
    const inputField = inputSetup('email');

    fireEvent.change(inputField, { target: { value: 'hello@athimannil.com' } });
    expect(inputField.value).toBe('hello@athimannil.com');
  });

  it('input email in miss matchng', () => {
    const inputField = inputSetup('email');

    fireEvent.change(inputField, { target: { value: 'hello@gmail.com' } });
    expect(inputField.value).not.toBe('hello@athimannil.com');
  });
});

describe('Input search', () => {
  it('input in valid search string', () => {
    const inputField = inputSetup('search');

    fireEvent.change(inputField, { target: { value: 'best language' } });
    expect(inputField.value).toBe('best language');
  });

  it('input text in miss matchng', () => {
    const inputField = inputSetup('email');

    fireEvent.change(inputField, { target: { value: 'good language' } });
    expect(inputField.value).not.toBe('bad language');
  });
});

test('Should render snapshot', () => {
  const onChangeValue = jest.fn();
  const { asFragment } = render(
    <ThemeProvider theme={Theme}>
      <Input type='text' id="name" onChange={onChangeValue} placeholder="name" value="hello mate" />
    </ThemeProvider>
  );

  expect(asFragment()).toMatchSnapshot();
});
