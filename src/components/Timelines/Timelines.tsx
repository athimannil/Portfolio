'use client';
import { FC, MouseEvent, useState } from 'react';
import { TimelinesProps } from './type';

import './style.scss';
import SpeechBubble from './speechBubble';

const Timelines: FC<TimelinesProps> = ({ items }) => {
  const [activeBubble, setActiveBubble] = useState<number>(1);

  console.log('----------items----------');
  console.log(items);

  // const toggleSpeechBubble = (event: MouseEvent<HTMLButtonElement>)=> {
  //   event.preventDefault();
  const toggleSpeechBubble = (itemIndex: number)=> {
    console.log('-----toggleSpeechBubble-----');
    console.log(itemIndex);
    setActiveBubble(itemIndex);
  // const toggleSpeechBubble = (index: string)=> {
    // let button = e.target as HTMLInputElement;
    // console.log(index);
  };

  if (items.length > 0 ) {
    return (
      <div className="timeline">
        {
          items.map(({ title, company, location, startDate, endDate, description }, index) => {
            console.log('index.... ', index);
            const active = index === activeBubble;
            return (
              <div className="timeline__item group" key={`${company}${index}`}>
                {/* <div className="timeline__item-date">{`${startDate} - ${endDate ? endDate : 'Present'}`}</div> */}
                <div className="timeline__item-date">
                  <span>{startDate}</span>
                  <span>-</span>
                  <span>{`${endDate ? endDate : 'Present'}`}</span>
                </div>
                <button className="timeline__item-button" onClick={() => toggleSpeechBubble(index)}>{active ? '-' : '+' }</button>
                <SpeechBubble
                  title={title}
                  company={company}
                  location={location}
                  startDate={startDate}
                  endDate={endDate}
                  description={description}
                  active={active}
                />
              </div>
            );
          })
        }
      </div>
    );
  }
  return null;
};

export default Timelines;
