export type SpeechBubbleProps = {
  title: string;
  company: string;
  location: string;
  startDate: string;
  endDate: string | null;
  description: string;
  active: boolean;
}


export type TimelinesProps = {
  items: SpeechBubbleProps[];
};
