import { useEffect, useRef } from 'react';
import './style.scss';

const Map = () => {
  useEffect(() => {
    const googleMapScript = document.createElement('script');
    googleMapScript.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDtwLGMk2mRH8pFkhJrRtJ0lTyT0PokK4Q&libraries=places';
    googleMapScript.async = true;
    window.document.body.appendChild(googleMapScript);
    googleMapScript.addEventListener('load', () => {
      getLatLng();
    });
  }, []);

  const getLatLng = () => {
    const myLatlng = new google.maps.LatLng(52.522756, 13.392999);
    const mapOptions = {
      zoom: 14,
      center: myLatlng,
      disableDefaultUI: false,
      mapTypeControl: false,
      disableDoubleClickZoom: true,
      scrollwheel: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      styles: [{
        stylers: [{
          hue: '#E16D65'
        }, {
          saturation: -40
        }]
      }, {
        elementType: 'geometry',
        stylers: [{
          lightness: 0
        }, {
          visibility: 'simplified'
        }]
      }, {
        featureType: 'landscape',
        stylers: [{
          lightness: 100
        }]
      }, {
        featureType: 'road',
        elementType: 'labels',
        stylers: [{
          visibility: 'off'
        }]
      },{
        featureType: 'road.arterial',
        elementType: 'geometry.fill',
        stylers: [{
          color: '#E16D65',
        }]
      },{
        featureType: 'road.local',
        stylers: [
          { color: '#ff8c86' },
          { lightness: 75 }
        ]
      },{
        featureType: 'administrative.locality',
        elementType: 'labels.text',
        stylers: [
          { color: '#666666' },
          { weight: 0.4 }
        ]
      },{
        featureType: 'poi.park',
        stylers: [
          { lightness: 100 }
        ]
      }
      ]};
    const map = new google.maps.Map(document.getElementById('my-map'), mapOptions);
    map.panBy(-150, 50);
    google.maps.event.addDomListener(window, 'scroll', function () {
      const scrollY = window.scrollY,
        scroll = map.get('scroll');
      if (scroll) {
        map.panBy(0, -((scrollY - scroll.y) / 2));
      }
      map.set('scroll', {
        y: scrollY
      });
    });
    google.maps.event.addDomListener(window, 'resize', function() {
      const center = map.getCenter();
      google.maps.event.trigger(map, 'resize');
      map.setCenter(center);
    });
  };
  return (
    <div id="my-map" className="map" />
  );
};

export default Map;
