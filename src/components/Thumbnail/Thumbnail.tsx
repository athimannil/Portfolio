import React, { FC } from 'react';
import Image from 'next/image';
import './style.scss';

type ThumbnailProps = {
  title?: string;
  image: string
};

const Thumbnail: FC<ThumbnailProps> = ({ title, image }) => {
  return (
    <figure className="thumbnail">
      <Image
        className="thumbnail__image"
        src={`/images/${image}`}
        alt={title ? title : ''}
        width={180}
        height={180}
      />
      {title && <figcaption className="thumbnail__title">{title}</figcaption>}
    </figure>
  );
};

export default Thumbnail;
