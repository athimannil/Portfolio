import React from 'react';
import './style.scss';

type LanguagesProps = {
  language: string;
  percentage: number;
}

type SkillsProps = {
  languages: LanguagesProps[];
};

const Skills: React.FC<SkillsProps> = ({ languages = [] }) => {
  if (languages.length === 0) return null;
  return (
    <ul className="skills">
      {
        languages.map(({ language, percentage }) =>
          <li key={language} className="skills__item" style={{ width: `${percentage}%` }}>{language}</li>)
      }
    </ul>
  );
};

export default Skills;
