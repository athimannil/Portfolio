import { render, screen } from '@testing-library/react';
import Skills from './Skills';

const mySkills = [
  {
    language: 'HTML',
    percentage: 75,
  },
  {
    language: 'CSS',
    percentage: 25,
  },
];
describe('Skills in various in states', () => {
  it('Skills with two skills', async () => {
    render(<Skills languages={mySkills} />);
    const items = await screen.findAllByRole('listitem');
    expect(items).toHaveLength(2);
  });

  it('Skills with empty skills', () => {
    const { container } = render(<Skills languages={[]} />);
    expect(container).toBeEmptyDOMElement();
  });

  it('render skills list', () => {
    const { asFragment } = render(<Skills languages={mySkills} />);
    expect(asFragment()).toMatchSnapshot();
  });
});
