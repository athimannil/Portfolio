import { FC } from 'react';
import Image from 'next/image';
import PortfolioThumbnail from '@/components/PortfolioThumbnail/PortfolioThumbnail';

import './style.scss';

const portfolioList = [
  {
    image: 'surface.png',
    title: 'Microsoft Surface',
    url: null,
    stacks: []
  },
  {
    image: 'foznol.png',
    title: 'Foznol',
    url: null,
    stacks: []
  },
  {
    image: 'holiday-inn.png',
    title: 'Holiday Inn',
    url: null,
    stacks: []
  },
  {
    image: 'ucl.png',
    title: 'University College of London',
    url: 'http://extend.ucl.ac.uk/',
    stacks: []
  },
  {
    image: 'ba.png',
    title: 'British Airways',
    url: 'http://www.gomolearning.com/"',
    stacks: []
  },
  {
    image: 'love-space.png',
    title: 'Love Space',
    url: 'http://www.lovespace.co.uk/"',
    stacks: []
  },
  {
    image: 'resolor.png',
    title: 'Resolor',
    url: null,
    stacks: []
  },
  {
    image: 'design-portfolio.png',
    title: 'Design Portfolio',
    url: 'http://www.design-portfolio.co.uk/',
    stacks: []
  },
];

const oldPortfolioList = [
  {
    image: 'bt.jpg',
    title: 'British Telecom',
    url: 'http://business.bt.com/',
    stacks: ['HTML5', 'CSS3', 'Sass', 'JavaScript', 'JSON']
  },
  {
    image: 'digimob.jpg',
    title: 'Digimob Group',
    url: 'http://www.digimobgroup.com/',
    stacks: ['HTML5', 'CSS3', 'Sass', 'JavaScript', 'JSON']
  },
  {
    image: 'hub.jpg',
    title: 'BT Web App',
    url: '',
    stacks: ['HTML5','CSS3','javaScript','jQuery'],
  },
  {
    image: 'digimobjobs.jpg',
    title: 'Digimob Jobs',
    url: '',
    stacks: ['HTML5','CSS3','jQuery','PHP5','MySQL'],
  },
  {
    image: 'flatui-admin.jpg',
    title: 'digimob admin panel',
    url: '',
    stacks: ['Bootstrap','Sass','CSS3','PHP5','MySQL'],
  },
  {
    image: 'smart-puff.jpg',
    title: 'Smart Puff',
    url: '',
    stacks: ['WordPress','HTML5','CSS3'],
  },
  {
    image: 'admin.jpg',
    title: 'Love City',
    url: '',
    stacks: [],
  },
  {
    image: 'woodgreen-films.jpg',
    title: 'Woodgreen Films',
    url: '',
    stacks: ['HTML5','CSS3','OOPHP','MySQL','JSON'],
  },
  {
    image: 'lovecity.jpg',
    title: 'Love City',
    url: '',
    stacks: ['HTML5','CSS3','jQuery','PHP5','MySQL'],
  },
];

const portfolio = () => {
  return (
    <div className="container">
      <section className="gallery gallery__responsive">
        {
          portfolioList.map(({image, title, stacks}) =>
            <PortfolioThumbnail key={title} responsive={false} image={image} title={title} stacks={stacks} />)
        }
      </section>
      <section className="gallery">
        {
          oldPortfolioList.map(({image, title, stacks}) => 
            <PortfolioThumbnail key={title} responsive image={image} title={title} stacks={stacks} />)
        }
      </section>
    </div>
  );
};

export default portfolio;
