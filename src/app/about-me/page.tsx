import React from 'react';
import Image from 'next/image';

import './style.scss';
import Skills from '@/components/Skills/Skills';
import Thumbnail from '@/components/Thumbnail/Thumbnail';

const languages = [
  {
    language: 'HTML5',
    percentage: 90,
  },
  {
    language: 'CSS3',
    percentage: 92,
  },
  {
    language: 'JavaScript',
    percentage: 69,
  },
  {
    language: 'jQuery',
    percentage: 81,
  },
  {
    language: 'Sass',
    percentage: 66,
  },
  {
    language: 'Bootstrap',
    percentage: 85,
  },
  {
    language: 'PHP',
    percentage: 62,
  },
  {
    language: 'MySQL',
    percentage: 56,
  },
];

const socialMedias = [
  {
    media: 'linkedIn',
    url: 'http://uk.linkedin.com/in/msathimannil/',
    username: '',
    color: '#0A66C2'
  }, {
    media: 'instagram',
    url: 'http://instagram.com/msathimannil',
    username: 'msathimannil',
    color: '#E4405F'
  }, {
    media: 'facebook',
    url: 'https://www.facebook.com/MSAthimannil',
    username: '',
    color: '#1877F2'
  }, {
    media: 'Google plus',
    url: 'https://plus.google.com/101426681494840024341/posts',
    username: '',
    color: '#d14836',
  }, {
    media: 'twitter',
    url: 'https://twitter.com/MSAthimannil',
    username: '',
    color: '#1D9BF0'
  }, {
    media: 'skype',
    url: 'https://twitter.com/MSAthimannil',
    username: 'athimannil',
    color: '#00AFF0'
  }
];

const About = () => {
  return (
    <>
      <section className="about">
        <h1 className="hidden">About me</h1>
        <article className="about__article">
          <h3 className="about__article-title">Who I am</h3>
          <p className="about__article-detail">I would like to introduce my self as a web developer. Since 2008, I have been working in web industry. I am passionate about producing creative, unique visuals that clearly communicate a brand’s message. Particularly those related to the arts or those that engage young people. I then transform them into clean, standards-based markup.</p>
          <p className="about__article-detail">I also have a keen interest in mobile / responsive web development and have recently started putting this into practice, as you can see by this very website. I work in front-end and back-end development with UI/UX designers by having them outsource work to me and also work with my own, direct clients as you can see by most of the pieces in my portfolio. I am happy to work either remotely or in-house, whichever works best.</p>
        </article>
        <div className="about__photo">
          <Thumbnail image="muhammed.jpg" title="Muhammed Athimannil" />
        </div>
        <article className="about__article">
          <h3 className="about__article-title">What I love</h3>
          <p className="about__article-detail">I love to create unique clear responsive sites that are user friendly. All my works comply with web standards, elegant with hand coded. I strive to write code of the highest standard and bug fix in an efficient and extensible way.</p>
          <p className="about__article-detail">I work for both front-end and back-end websites for over 4 years. some of them I built using WordPress CMS, which is the most used content management system in the world. Every website is tailored to client needs and created from scratch without using any templates.</p>
          <p className="about__article-detail">I developed websites for bigger companies as part of a team, if you need examples of those websites - drop me a line</p>
        </article>
        <div className="about__illustrate">
          <h3 className="about__article-title">Skills</h3>
          <Skills languages={languages} />
        </div>
      </section>
      <aside className="container mt-12">
        <h3 className="text-lg mb-4">Social media</h3>
        <ul className="socialmedia">
          {
            socialMedias.map(({ media, url, color, username }) =>
              <li key={media} className="socialmedia__list">
                <a
                  className="socialmedia__list-link"
                  href={url}
                  style={{ backgroundColor: `${color}` }}
                  target="_blank"
                >
                  <Image
                    className="socialmedia__list-image"
                    src={`/images/${media}.svg`}
                    alt={media}
                    width={40}
                    height={40}
                  />
                  {media}
                </a>
              </li>
            )
          }
        </ul>
      </aside>
    </>
  );
};

export default About;
